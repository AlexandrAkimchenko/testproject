﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenuController : MonoBehaviour
{
    public Text PlayerName;
    public Text score;
    public GameObject menu;
    public GameObject send;
    public GameObject button;
    public Text inputText;

    private void Start()
    {
        Global.gameOver.AddListener(GameOver);
    }
    private void GameOver()
    {
        menu.SetActive(true);
        score.text = "Score : " + Global.score;
    }
    private void Update()
    {
        PlayerName.text = "Name player : "+inputText.text;
    }
    public void SaveScore()
    {
        PlayerScore player = new PlayerScore();
        player.namePLayer = inputText.text.ToString();
        player.score = Global.score;
        SaveManager.Instance.state.players.Add(player);
        SaveManager.Instance.Save();
        send.SetActive(false);
        button.SetActive(true);
    }
}
