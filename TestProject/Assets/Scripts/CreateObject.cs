﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateObject : MonoBehaviour
{
    public GameObject enemy;
    public float minTameCreatEnemy;
    public float mxaTameCreatEnemy;

    public GameObject bonus;
    public float createBonusTime;

    void Start()
    {
        StartCoroutine(CreateEnemy());
        StartCoroutine(CreateBonus());
        Global.gameOver.AddListener(GameOver);
    }
    public void GameOver()
    {
        StopAllCoroutines();
    }

    IEnumerator CreateEnemy()
    {
        while (!Global.stopGame)
        {
            yield return new WaitForSecondsRealtime(Random.Range(minTameCreatEnemy, mxaTameCreatEnemy));
            Instantiate(enemy, new Vector3(0, 0, 0), Quaternion.identity);
        }
    }
    IEnumerator CreateBonus()
    {
        while (!Global.stopGame)
        {
            yield return new WaitForSecondsRealtime(createBonusTime);
            Instantiate(bonus, new Vector3(-2, 4, 0), Quaternion.identity);
        }
    }

}
