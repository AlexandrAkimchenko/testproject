﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance { set; get; }
    public SaveState state;

    private void Start()
    {
        Instance = this;
        Load();
    }
    public void Save()
    {
        XmlSerializer xml = new XmlSerializer(typeof(SaveState));
        StringWriter writer = new StringWriter();
        xml.Serialize(writer, state);
        PlayerPrefs.SetString("save", writer.ToString());
    }
    public void Load()
    {
        if (PlayerPrefs.HasKey("save"))
        {
            XmlSerializer xml = new XmlSerializer(typeof(SaveState));
            StringReader reader = new StringReader(PlayerPrefs.GetString("save"));
            state = (SaveState)xml.Deserialize(reader);
        }
        else
        {
            state = new SaveState();
            state.players = new List<PlayerScore>();
            Save();
        }
    }
}
