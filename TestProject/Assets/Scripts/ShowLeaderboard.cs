﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowLeaderboard : MonoBehaviour
{
    RectTransform rectTransform;
    private int iter;
    public GameObject pref;
    private LeaderboardElement leaderboardElement;

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        foreach (PlayerScore player in SaveManager.Instance.state.players)
        {
            iter++;
            rectTransform.sizeDelta = new Vector2(0,150*iter);
            leaderboardElement = Instantiate(pref,gameObject.transform).GetComponent<LeaderboardElement>();
            if (player.namePLayer.Equals("")) {
                leaderboardElement.namePlayer.text = "no name";
            }
            else {
                leaderboardElement.namePlayer.text = player.namePLayer;
            }
            leaderboardElement.score.text = player.score.ToString();
        }
    }

}
