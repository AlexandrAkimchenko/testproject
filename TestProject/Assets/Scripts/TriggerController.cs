﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerController : MonoBehaviour
{
    private Bonus bonus;
    private bool invulnerability;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            if (!invulnerability)
            {
                Global.stopGame = true;
                Global.gameOver.Invoke();
            }
            else
            {
                Destroy(col.gameObject);
            }
        }
        else if (col.tag == "Bonus")
        {
            bonus = col.GetComponent<Bonus>();
            StartCoroutine(Bonus(bonus.timeOfAction, bonus.typeBonus));

        }
    }



	IEnumerator Bonus(float timeOfAction, TypeBonus typeBonus)
    {
        switch (typeBonus)
        {
            case TypeBonus.xScore:
                Global.xScore = 10;
                break;
            case TypeBonus.speedUp:
                Global.upSpeed.Invoke(5);
                break;
            case TypeBonus.invulnerability:
                invulnerability = true;
                break;
        }
        Destroy(bonus.gameObject);
        yield return new WaitForSecondsRealtime(timeOfAction);

        switch (typeBonus)
        {
            case TypeBonus.xScore:
                Global.xScore = 1;
                break;
            case TypeBonus.speedUp:
                Global.startSpeed.Invoke();
                break;
            case TypeBonus.invulnerability:
                invulnerability = false;
                break;
        }
            
    }
}
