﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float gameTime;
    private Text text;
    private int second = 0;
    private int minut = 0;
    public int pointPerSecond;

    private void Start()
    {
        text = GetComponent<Text>();
        Global.score = 0;
        Global.xScore = 1;
    }
    void Update()
    {
        gameTime += Time.deltaTime;
        if (!Global.stopGame && gameTime >= 1)
        {
            gameTime -= 1;
            second++;
            Global.score += pointPerSecond * Global.xScore;
            if (second == 10)
            {
                Global.score += pointPerSecond * Global.xScore;
            }
            if (second > 60)
            {
                second = 0;
                minut++;
            }
            text.text = minut + ":" + second;
        }
    }
}
