﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    public float timeOfAction  { get; private set; }
    public TypeBonus typeBonus { get; private set; }
    public  float timeLife  { get; private set; }

    void Start()
    {
        timeLife = 8;
        timeOfAction = 10;
        StartCoroutine(Life());
        typeBonus = (TypeBonus)Random.Range(0, 2);
    }

    void Update()
    {
        
    }
    IEnumerator Life()
    {
        yield return new WaitForSecondsRealtime(timeLife);
        Destroy(gameObject);
    }

}
public enum TypeBonus
{
    xScore,
    speedUp,
    invulnerability
}