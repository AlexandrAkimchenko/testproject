﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Global 
{
    public static Vector3 playerPosition;
    public static bool stopGame;
    public static int score;
    public static int xScore = 1;
    public static float enemySpeed;
    public static UnityEvent startSpeed = new UnityEvent();
    public static MyEvent upSpeed = new MyEvent();
    public static UnityEvent gameOver = new UnityEvent();
}
[System.Serializable]
public class MyEvent : UnityEvent<float> { }