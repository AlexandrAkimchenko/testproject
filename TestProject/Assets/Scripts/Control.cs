﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Enemy", menuName = "Control")]
public class Control : ScriptableObject
{
    public bool isPlayer;
    public float speed;
    public float upSpeedConfidet;
    public float upSpeedTime;
}
