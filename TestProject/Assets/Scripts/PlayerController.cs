﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Control control;
    Vector3 position;
    float angle;
    private float mySpeed;
    void Start()
    {

        position = transform.position;
        mySpeed = control.speed;
        if (!control.isPlayer)
        {
            StartCoroutine(SpeedUp());

            if (mySpeed < Global.enemySpeed)
            {
                mySpeed = Global.enemySpeed;
            }
        }
        else
        {
            Global.playerPosition = transform.position;
        }
        Global.upSpeed.AddListener(UpSpeed);
        Global.startSpeed.AddListener(StartSpeed);
        Global.gameOver.AddListener(GameOver);

    }
    void Update()
    {
        if (control.isPlayer) {
            if (Input.touchCount > 0) {
                Touch touch = Input.GetTouch(0);

                Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                Vector3 vect = touch.position;
                touchPosition.z = 0;
                position = touchPosition;
                var dir = vect - Camera.main.WorldToScreenPoint(transform.position);
                angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                Global.playerPosition = position;
            }
            transform.position = Vector3.Lerp(transform.position, position, mySpeed * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), Time.deltaTime * 5);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, Global.playerPosition, mySpeed * Time.deltaTime);
            var dir = Camera.main.WorldToScreenPoint(Global.playerPosition) - Camera.main.WorldToScreenPoint(transform.position);
            angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), Time.deltaTime * 5);

        }
    }
    public void UpSpeed(float newSpeed)
    {
        if (control.isPlayer)
        {
            mySpeed = newSpeed;
        }
    }
    public void StartSpeed()
    {
        if (control.isPlayer)
        {
            mySpeed = control.speed;
        }
    }

    public void GameOver()
    {
        mySpeed = 0;
        StopAllCoroutines();
    }
    IEnumerator SpeedUp()
    {
        while (!Global.stopGame)
        {
            yield return new WaitForSecondsRealtime(control.upSpeedTime);
            mySpeed += control.upSpeedConfidet;
            if (mySpeed > Global.enemySpeed)
            {
                Global.enemySpeed = mySpeed;
            }
        }
    }
}
